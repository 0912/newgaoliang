// PB2H.cpp : Defines the entry point for the console application.
//加快速度发收到看是 🎩👑👒🌂💼👜💄💍💎☕🍵 🍺🍻🍸🍶🍴🍔

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}
// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}
// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}


// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}
// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}


// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}
// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}


// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}
// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);
	
	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength()-1)*sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_WINDOWS) && (D_TARGET_WINDOWS==1)
	DStringA strWin = pfile.TranslateLineForWin();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + DString(L".h");
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

	//Write Ansi output
	//DBuffer bufw_win((DByte*)strWin.GetStr(), strWin.GetDataLength());
	//of_win.Write(bufw_win);

	//We write Unicode output
	DString strWinU = strWin.ToUnicode();
	DBuffer bufw_win((DByte*)strWinU.GetStr(), (strWinU.GetDataLength() - 1) * sizeof(TCHAR));
	DBuffer u16head(2);
	u16head.SetAt(0, 0xFF);
	u16head.SetAt(1, 0xFE);
	of_win.Write(u16head);
	of_win.Write(bufw_win);

	printf("Save to file: %s\n", strPathWin.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_IOS) && (D_TARGET_IOS==1)
	DStringA striOS = pfile.TranslateLineForiOS();
	DString strPathiOS = strPathW + L".h";
	DFile of_ios;
	of_ios.OpenFileWrite(strPathiOS.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_ios((DByte*)striOS.GetStr(), striOS.GetDataLength());
	of_ios.Write(bufw_ios);
	printf("Save to file: %s\n", strPathiOS.ToAnsi().GetStr());
#endif

#if defined(D_TARGET_ANDROID) && (D_TARGET_ANDROID==1)
	DStringA strAndroid = pfile.TranslateLineForAndroid();
	DStringA strPathAndroid = pfile.GetPackageName();
	strPathAndroid = pfile.m_strDirPath.ToAnsi() + strPathAndroid + ".java";
	DFile of_android;
	of_android.OpenFileWrite(strPathAndroid, DFILE_CREATE_ALWAYS);
	DBuffer bufw_android((unsigned char*)(LPCSTR)strAndroid,strAndroid.GetDataLength());
	of_android.Write(bufw_android);
	printf("Save to file: %s\n", strPathAndroid.GetStr());
#endif

	//Step 6: Output Map
	DStringA strMap = pfile.GetSigMapStr();
	DString strPathMap = strPathW + DString(L".map.txt");
	DFile of_map;
	of_map.OpenFileWrite(strPathMap.ToAnsi(), DFILE_CREATE_ALWAYS);
	DBuffer bufw_map((unsigned char*)(LPCSTR)strMap,strMap.GetDataLength());
	of_map.Write(bufw_map);
	printf("Save Map to file: %s\n", strPathMap.ToAnsi().GetStr());

	system("pause");
	return 0;
}

// PB2H.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "DMemAlloc.h"
#include "DString.h"
#include "DFile.h"
#include "DPBFile.h"
#include "DConfig.h"
#include "DTypes.h"

#define D_TARGET_WINDOWS_OLD 0
#define D_TARGET_WINDOWS 1
#define D_TARGET_IOS 0
#define D_TARGET_ANDROID 0

int main(int argc, char* argv[])
{
	//argv[1] should be the file path
	if (argc == 1)
	{
		printf("Need PB File\n");
		return 0;
	}

	//Step 0: Open File
	DStringA strPath(argv[1]);
	DString strPathW(strPath.ToUnicode());
	DPBFile pfile;
	DBool bResult = pfile.OpenFile(strPathW);
	if (!bResult)
	{
		DPrintf("Open File %s Error\n", strPath.GetStr());
	}
	else
	{
		DPrintf("File Path: %s Size:%d\n", strPath.GetStr(), pfile.GetFileSize());
	}

	//Step 1: PreProcess to support import
	bResult = pfile.PreProcess();
	if (!bResult)
	{
		DPrintf("PreProcess Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 1 PreProcess OK\n");	
		pfile.OutputSubfiles();
	}

	//Step 2: process g_subfiles
	bResult = pfile.ProcessHeaders();
	if (!bResult)
	{
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 2 ProcessHeaders OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);		
	}

	//Step 3: Parse self
	bResult = pfile.Parse();
	if (!bResult)
	{
		DPrintf("Parse File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 3 Parse Self OK\n");
		DPrintf("current Max Mid = %u\n", pfile.m_curMaxMid);
	}

	//Step 4: Trans self
	bResult = pfile.Trans();
	if (!bResult)
	{
		DPrintf("Trans File Error\n");
		system("pause");
		return 0;
	}
	else
	{
		DPrintf("Step 4 Trans OK\n");
	}

	//Step 5: Translate File
	pfile.GenLine();

#if defined(D_TARGET_WINDOWS_OLD) && (D_TARGET_WINDOWS_OLD==1)
	DStringA strWin = pfile.TranslateLine();
	int nLen = strWin.GetDataLength();
	int nLen2 = strWin.CalStrLen();
	DString strPathWin = strPathW + L".h";
	DFile of_win;
	of_win.OpenFileWrite(strPathWin.ToAnsi(), DFILE_CREATE_ALWAYS);

